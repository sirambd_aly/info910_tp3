#!/usr/bin/env python
# -*- coding: utf-8 -*-
from PIL import Image

import Steganographie

if __name__ == "__main__":
    import sys

    if len(sys.argv) != 3:
        print("usage: {} image size".format(sys.argv[0]))
        sys.exit(1)

    img = Image.open(sys.argv[1])
    Steganographie.read_message(img, int(sys.argv[2]))
