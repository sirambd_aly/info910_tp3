#!/usr/bin/env python
# -*- coding: utf-8 -*-
import Degree
import GenerateKey

if __name__ == "__main__":
    """
    Check if the hex-signature is valid
    """
    import sys

    if len(sys.argv) != 2:
        print("usage: {} hex_signature".format(sys.argv[0]))
        sys.exit(1)

    if GenerateKey.verification_hex_signing('../output/public.pem',
                                            'degree)8^f=^n!+fehxf=zqq7_ib9h!&)*a1tl^^9uy_&i2x+s1($t61', sys.argv[1]):
        print("Is valid")
    else:
        print("In not valid")
