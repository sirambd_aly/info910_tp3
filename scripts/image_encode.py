#!/usr/bin/env python
# -*- coding: utf-8 -*-
import Steganographie

if __name__ == "__main__":
    import sys

    if len(sys.argv) != 4:
        print("usage: {} image message output".format(sys.argv[0]))
        sys.exit(1)

    Steganographie.main(sys.argv[1], sys.argv[2], sys.argv[3])
    print("{} enregistrer avec succès".format(sys.argv[3]))
