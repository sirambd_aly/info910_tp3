#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
from os import path

import GenerateKey

if __name__ == "__main__":
    if not path.isdir('./output'):
        os.mkdir('./output')

    GenerateKey.generate_public_key()
    print("Your public key generate with success in output folder")
    GenerateKey.generate_private_key()
    print("Your private key generate with success in output folder")
