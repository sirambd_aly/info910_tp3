#!/usr/bin/env python
# -*- coding: utf-8 -*-
import Degree

font = '../fonts/sans.ttf'
if __name__ == "__main__":
    import sys

    if len(sys.argv) != 4:
        print("usage: {} name score output".format(sys.argv[0]))
        sys.exit(1)

    name = sys.argv[1]
    score = int(sys.argv[2])
    output = sys.argv[3]
    res_image = Degree.add_signature_image("../input/diplome-BG.png",
                                           'degree)8^f=^n!+fehxf=zqq7_ib9h!&)*a1tl^^9uy_&i2x+s1($t61', output,
                                           "../output/private.pem", font=font)
    Degree.add_score(res_image, name, score, output, font=font)
    print("Degree generated successfully!")
