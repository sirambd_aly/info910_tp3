import qrcode


# from qrcode.image.pure import PymagingImage


def qr_code_generator(message):
    qr = qrcode.QRCode(
        version=12,
        error_correction=qrcode.constants.ERROR_CORRECT_H,
        box_size=2,
        border=8
    )
    qr.add_data(message)
    qr.make()
    img = qr.make_image()
    # img.save(output)
    return img


if __name__ == "__main__":
    qr_code_generator("https://www.lama.univ-smb.fr/TPLab/liste_TP/info-910/")
