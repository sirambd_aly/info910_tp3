from PIL import Image

message_size = 8  # nombre d'octets autorisé pour le message


def hide_message(img, message):
    m = img.height // 2  # milieu de l'image
    pixels = img.load()  # tableau des pixels
    message_encoded = message.encode()
    index_current_bytes = 0  # octet
    index_current_byte = 0  # bite
    for x in range(0, img.width):
        r, g, b = pixels[x, m]  # on rÃ©cupÃ¨re les composantes RGB du pixel (x,m)
        if index_current_bytes < len(message):
            r = (r & 0b11111110)
            r = r | ((message_encoded[index_current_bytes] >> index_current_byte) & 1)
            index_current_byte = (index_current_byte + 1) % 8
            index_current_bytes = index_current_bytes + 1 if index_current_byte == 0 else index_current_bytes
            pixels[x, m] = r, g, b  # on remet les pixels inversÃ©s dans le tableau
        else:
            break


def read_message(img, size):
    m = img.height // 2  # milieu de l'image
    pixels = img.load()  # tableau des pixels
    message = []
    current_bytes = ['0'] * 8  # un tableau qui représente l'octet courant
    index_current_byte = 7  # où insérer le bite
    count = 0  # contient le nombre d'octets déjà parcouru
    for x in range(0, img.width):
        r, g, b = pixels[x, m]  # on rÃ©cupÃ¨re les composantes RGB du pixel (x,m)
        current_bytes[index_current_byte] = str(r & 1)
        index_current_byte = (index_current_byte - 1) % 8
        if index_current_byte == 0 and count < size:
            message.append(int("".join(current_bytes), 2))
            count += 1
            current_bytes = ['0'] * 8
        elif count > size:
            break
    print(''.join(map(chr, message)))


def main(filename, message, output):
    img = Image.open(filename)  # ouverture de l'image contenue dans un fichier
    hide_message(img, message)
    img.save(output)  # sauvegarde de l'image obtenue dans un autre fichier


if __name__ == "__main__":
    import sys

    if len(sys.argv) != 4:
        print("usage: {} image message output --encode|--decode".format(sys.argv[0]))
        sys.exit(1)
    main(sys.argv[1], sys.argv[2], sys.argv[3])
