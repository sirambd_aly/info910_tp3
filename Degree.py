import AddTextToImage
import GenerateKey
import QrcodeManager

DEFAULT_OUTPUT = "output/"


def add_signature_image(img, msg_sign, output, private_key="output/private.pem", font="fonts/sans.ttf"):
    file_out = open(output + ".txt", "w")
    signing_msg = GenerateKey.signing(private_key, msg_sign)
    signing_msg_hex = signing_msg.hex()
    file_out.write(signing_msg_hex)
    file_out.flush()
    file_out.close()
    signing_msg_hex = [signing_msg_hex[i:i + 35] for i in range(0, len(signing_msg_hex), 35)]
    res_image = AddTextToImage.save_text_to_image(img, signing_msg_hex, output, x=3, y=1, font_size=8, font=font)
    add_qrcode(res_image, signing_msg, output)
    # GenerateKey.verification_hex_signing("output/public.pem", "ok", "output/image_output.txt", True)
    return res_image


def add_score(img, pseudo, score, output, font="fonts/sans.ttf"):
    res_image = AddTextToImage.save_text_to_image(img, "Attestation de réussite", output, x=125, y=300, font_size=70,
                                                  font=font)
    AddTextToImage.save_text_to_image(res_image, "{} a fini avec un score de {}".format(pseudo, score), output,
                                      x=250, y=390, font_size=30, font=font)
    return res_image


def add_qrcode(img, value, output):
    imqr = QrcodeManager.qr_code_generator(value, )
    AddTextToImage.add_image_to_image(img, imqr, output, 745, 0)


if __name__ == "__main__":
    res_image = add_signature_image("input/diplome-BG.png", "ok", "output/image_output.png")
    res_image = add_score(res_image, "sirambd", 30, "output/image_output.png")
