#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Main script to run the degree management web-app.
Usage:
   run.py <port> <mode>

Options:
    -h --help                   Show this screen.
    <port>                      Port to run the server example 8080, [default 8080]
    <mode>                      debug or prod, else prod
"""

import GenerateKey
from web import app
import os
from os import path
from docopt import docopt

if __name__ == "__main__":
    args = docopt(__doc__)
    if not path.isdir('./output'):
        os.mkdir('./output')

    try:
        port = int(args["<port>"])
    except ValueError:
        port = 8080
    try:
        debug_mode = args["<mode>"] == 'debug'
    except ValueError:
        debug_mode = False

    GenerateKey.generate_private_key()
    GenerateKey.generate_public_key()
    app.run(debug=debug_mode, port=port)
