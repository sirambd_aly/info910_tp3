#Info910_TP3

```
Abdourahamane Boinaidi
Aly-Nayef Basma
TP3 - INFO910
professeur: Pierre Hyvernat
```

## Installation

Install all requirements
```
pip install -r requirements.txt
```

# Quick start

Go to scripts folder for the quick start script, but you can start with the web page.
On the web page you can create and validate degree. Go to the web part for more information...

## Create keys
To generate rsa private key and public key you need to run the following script
```
gen_rsa_keys.py
```

## Test steganographie
To add a text to an image, you need to run the following script
```
image_encode.py message
```
Exemple: ```../input/chablais-orig.png sirambd ../output/chablais-orig-out.png```

To extract the message from the image
```
image_decode.py size
```
Exemple: ```../output/chablais-orig-out.png 7```

## Generate a degree
To generate a degree, you need to run the following script
```
gen-degree.py name score output
```
Exemple: ```sirambd 30 ../output/out.png```

- When degree created, you can found it in output directory, with his file .txt who
contains the hex of signature.

## Validation degree
To check if the degree is valid, you need to copy his signature and run this
```check_degree.py hex_signature```

# Web
You can create and valid degree from the web
run this
```
run.py port mode
Options:
 - port: the port
 - mode: debug|prod
 ```
 
## Qr-code
You need to scan the qr-code, and copy the signature on this web application

You can just run this: ```run.py```