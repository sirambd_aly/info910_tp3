from Crypto.Cipher import PKCS1_OAEP, AES
from Crypto.Hash import SHA3_512, SHA3_256, MD5
from Crypto.PublicKey import RSA
from Crypto.Random import get_random_bytes
from Crypto.Signature import pkcs1_15

key = RSA.generate(2048)


def generate_private_key():
    key_private = key.export_key()
    file_out = open("output/private.pem", "wb")
    file_out.write(key_private)


def generate_public_key():
    key_public = key.publickey().export_key()
    file_out = open("output/public.pem", "wb")
    file_out.write(key_public)


def encrypt_data(message):
    data = message.encode("utf-8")
    file_out = open("output/encrypted_data.bin", "wb")

    recipient_key = RSA.import_key(open("output/public.pem").read())
    session_key = get_random_bytes(16)

    # Encrypt the session key with the public RSA key
    cipher_rsa = PKCS1_OAEP.new(recipient_key)
    enc_session_key = cipher_rsa.encrypt(session_key)

    # Encrypt the data with the AES session key
    cipher_aes = AES.new(session_key, AES.MODE_EAX)
    cipher_text, tag = cipher_aes.encrypt_and_digest(data)
    [file_out.write(x) for x in (enc_session_key, cipher_aes.nonce, tag, cipher_text)]


def decrypt_data(encrypted_path):
    file_in = open(encrypted_path, "rb")

    key_private = RSA.import_key(open("output/private.pem").read())
    enc_session_key, nonce, tag, ciphertext = \
        [file_in.read(x) for x in (key_private.size_in_bytes(), 16, 16, -1)]

    # Decrypt the session key with the private RSA key
    cipher_rsa = PKCS1_OAEP.new(key_private)
    session_key = cipher_rsa.decrypt(enc_session_key)

    # Decrypt the data with the AES session key
    cipher_aes = AES.new(session_key, AES.MODE_EAX, nonce)
    data = cipher_aes.decrypt_and_verify(ciphertext, tag)
    print(data.decode("utf-8"))


def signing(path_private_key, message):
    key_private = RSA.import_key(open(path_private_key).read())
    h = MD5.new(message.encode())
    # print(pkcs1_15.new(key_private).sign(h))
    return pkcs1_15.new(key_private).sign(h)


def verification_signing(path_public_key, message, sign_message):
    key_public = RSA.import_key(open(path_public_key).read())
    h = MD5.new(message.encode())

    try:
        pkcs1_15.new(key_public).verify(h, sign_message)
        print("The signature is valid.")
        return True
    except (ValueError, TypeError):
        print("The signature is not valid.")
        return False


def verification_hex_signing(path_public_key, message, signing_path, is_file=False):
    """
    Check if the signature is valid
    :param path_public_key:
    :param message: The signature key
    :param signing_path:
    :param is_file: True if the signing_path is a file else False
    :return:
    """
    if is_file:
        return verification_signing(path_public_key, message, bytes.fromhex(open(signing_path).read()))
    else:
        return verification_signing(path_public_key, message, bytes.fromhex(signing_path))


if __name__ == "__main__":
    generate_private_key()
    generate_public_key()
    encrypt_data("ça marche")
    decrypt_data("output/encrypted_data.bin")
    sign_msg = signing("output/private.pem", "ok")
    verification_signing("output/public.pem", "ok", sign_msg)
    verification_hex_signing("output/public.pem", "ok", "output/output.txt", is_file=True)
