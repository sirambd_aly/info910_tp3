from os import path

from flask import Flask, render_template, url_for, request, send_file

import Degree
import GenerateKey

app = Flask(__name__)

app.config.from_object('config')

# To get one variable, tape app.config['MY_VARIABLE']
signature_key = app.config['SIGNATURE_KEY']
image_output = "image_output.png"
user_name = "Hyvernat Pierre"
description = "description"


@app.route('/')
def welcome():
    return render_template('index.html',
                           user_name=user_name,
                           user_image=url_for('static', filename='img/profile.png'),
                           description="Cette page a pour but de montrer le résultat de notre TP, vous pouvez créer un diplome, le valider, et plus encore."
                                       "Pour l'instant il n'ya que la page Accueil et Diplome qui marche",
                           blur=False,
                           page_title="index")


@app.route('/degree/')
def degree_index():
    return render_template('degree.html',
                           user_name=user_name,
                           user_image=url_for('static', filename='img/profile.png'),
                           description="Cette page, sert à créer un diplôme, ou le vérifier.",
                           blur=False,
                           page_title="degree"
                           )


@app.route('/degree/create/', methods=['POST'])
def degree_create():
    name = request.form["player_name"]
    score = request.form["player_score"]
    res_image = Degree.add_signature_image("input/diplome-BG.png", signature_key, "output/" + image_output)
    Degree.add_score(res_image, name, score, "output/image_output.png")
    return send_file(path.join('..', 'output', image_output), as_attachment=True)


@app.route('/degree/validate/', methods=['POST'])
def degree_validate():
    hex_signature = request.form['hex_signature']
    if GenerateKey.verification_hex_signing('output/public.pem', signature_key, hex_signature):
        return "This signature is valid"
    else:
        return "This signature is not valid"


@app.route('/qrcode/<signature>/')
def qrcode_validate(signature):
    if GenerateKey.verification_signing('output/public.pem', signature_key, signature):
        return "This signature is valid"
    else:
        return "This signature is not valid"


# @app.route('/sir/<string:name>/')
# def sir(name):
#     return 'sir%s' % name


if __name__ == "__main__":
    app.run()
