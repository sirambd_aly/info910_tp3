from PIL import Image, ImageDraw, ImageFont

DEFAULT_OUTPUT = "output/"


def add_text(img, text, x, y, font_size, font="fonts/sans.ttf"):
    draw = ImageDraw.Draw(img)  # objet "dessin" dans l'image
    font = ImageFont.truetype(font, font_size)  # police à utiliser
    if isinstance(text, list):
        h = 0
        for t in text:
            draw.text((x, y + h), t, "black", font)  # ajout du texte
            h += 10
    else:
        draw.text((x, y), text, "black", font)  # ajout du texte


def save_text_to_image(filename, text, output, x=100, y=110, font_size=20, is_default_path=True, font="fonts/sans.ttf"):
    img = Image.open(filename)  # ouverture de l'image contenue dans un fichier
    add_text(img, text, x, y, font_size, font)
    if is_default_path:
        output = output if DEFAULT_OUTPUT in output else DEFAULT_OUTPUT + output
    img.save(output)  # sauvegarde de l'image obtenue dans un autre fichier
    return output


def add_image_to_image(filename, image, output, pos_x, pos_y, is_default_path=True):
    img = Image.open(filename)
    back_im = img.copy()
    back_im.paste(image, (pos_x, pos_y))
    if is_default_path:
        output = output if DEFAULT_OUTPUT in output else DEFAULT_OUTPUT + output
    back_im.save(output)


if __name__ == "__main__":
    import sys

    if len(sys.argv) != 4:
        print("usage: {} image msg output".format(sys.argv[0]))
        sys.exit(1)
    save_text_to_image(sys.argv[1], sys.argv[2], sys.argv[3])
