from PIL import Image

text = "Lorem ipsum"
encodeText = text.encode()
binText = ''.join(format(ord(x), 'b') for x in text)


def hide_message(img):
    m = img.height // 2  # milieu de l'image
    pixels = img.load()  # tableau des pixels
    index = 0
    message = []
    for x in range(0, len(encodeText)):
        print(encodeText[x])
        r, g, b = pixels[x, m]  # on rÃ©cupÃ¨re les composantes RGB du pixel (x,m)
        r = encodeText[x]
        message.append(r)
        pixels[x, m] = r, g, b  # on remet les pixels inversÃ©s dans le tableau
    print(''.join(str(e) for e in message))


def read_message(img):
    m = img.height // 2  # milieu de l'image
    pixels = img.load()  # tableau des pixels
    message = []
    for x in range(0, img.width):
        r, g, b = pixels[x, m]  # on rÃ©cupÃ¨re les composantes RGB du pixel (x,m)
    print(''.join(str(e) for e in message))


def main(filename, output):
    img = Image.open(filename)  # ouverture de l'image contenue dans un fichier
    hide_message(img)
    read_message(img)
    img.save(output)  # sauvegarde de l'image obtenue dans un autre fichier


if __name__ == "__main__":
    import sys

    if len(sys.argv) != 3:
        print("usage: {} image output".format(sys.argv[0]))
        sys.exit(1)
    main(sys.argv[1], sys.argv[2])
